var connection = require('./ConnectionMySql');

addCourses = (data) => {
    return new Promise(function (resolve, reject) {
        const sql = `INSERT INTO courses SET ?`;
        connection.query(sql,data, function (err, result) {
            if (err) reject(err);
            resolve(result.insertId);
        });
    });
};
module.exports.addCourses = addCourses;
