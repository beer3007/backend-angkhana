var connection = require('./ConnectionMySql');

addVideo = (data) => {
    return new Promise(function (resolve, reject) {
        const sql = `INSERT INTO videos SET ?`;
        connection.query(sql,data, function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.addVideo = addVideo;
