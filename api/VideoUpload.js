
const ffmpeg = require('ffmpeg');
const connection = require('../database/ConnectionMySql');
const coursesDB = require('../database/Courses');
const videoDB = require('../database/video');
const fileSystem = require('../component/file');
const R = require('ramda');
var moment = require('moment');

const VideoUpload = (req, res) => {
    const file = req.files.file;
    let output = {
        title: file.name,
        course_id: '',
        order: 1,
        thumbnail: '',
        duration: 0,
        quality_360: '',
        quality_480: '',
        quality_720: '',
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
    }

    connection.beginTransaction(function (err) {
        if (err) { throw err; }

        let data = {
            user_id: 1,
            title: 'My Courses'
        }
        coursesDB.addCourses(data).then(coursesId => {
            output.course_id = coursesId;
            const filename = `video-courses-${coursesId}.mp4`;
            fileSystem.scanFolder(coursesId).then(scan => {
                if (!scan) {
                    fileSystem.createFolder(`./public/video/${coursesId}`);
                }
                fileSystem.uploadFile(file, coursesId, filename).then(isUpload => {
                    fileSystem.createSnapshot(filename, coursesId).then(thumbnail => {
                        output.thumbnail = thumbnail.name;
                        output.duration = thumbnail.duration;
                        fileSystem.convertVideo(filename, coursesId).then(fileConvert => {
                            output.quality_360 = fileConvert.p360;
                            output.quality_480 = fileConvert.p480;
                            output.quality_720 = fileConvert.p720;
                            videoDB.addVideo(output).then(videoId => {
                                // console.log(videoId)
                                output.videoId = videoId;
                                connection.commit();
                                res.send({
                                    status: 200,
                                    data: {
                                        video: output,
                                        courses: coursesId
                                    }
                                })
                            }).catch(error => {
                                console.log(error);
                                connection.rollback();
                            })
                        }).catch(error => {
                            console.log(error);
                            connection.rollback();
                        })
                        // 
                    }).catch(error => {
                        console.log(error);
                        connection.rollback();
                    })
                }).catch(error => {
                    console.log(error);
                    connection.rollback();
                })
            }).catch(error => {
                console.log(error);
                connection.rollback();
            });
        }).catch(error => {
            console.log(error);
            connection.rollback();
        });
    });
}

module.exports = VideoUpload;