const logout =  (req, res) => {
  req.logout();
  res.send("logout");
}

module.exports = logout;