const jwt = require("jwt-simple");

const user = require('../database/Users');
const SECRET_KEY = process.env.SECRET_KEY;

const login =  (req, res) => {
  user
    .getUser(req.body.email, req.body.password)
    .then(userInfo => {
      if (userInfo.length > 0) {

        const payload = {
          sub: process.env.SECRET_KEY,
          iat: new Date().getTime()
        };

        const output = {
          status: 200,
          data: {
            token: jwt.encode(payload, SECRET_KEY),
            user: userInfo
          }
        };

        res.send(output);
      }else{
        res.send({
          status:403,
          message:"Wrong username and password"
        });        
      }
    })
    .catch(error => {
      res.send({
        status:500,
        message:"Internet Server Error."
      });
    });
}

module.exports = login;