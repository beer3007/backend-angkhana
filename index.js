const dotenv = require('dotenv');
dotenv.config();

const app = require('./config/router');


app.listen(3000, () => console.log("Example app listening on port 3000!"));