
const ffmpeg = require('ffmpeg');
const fs = require('fs');

const HD = {
    p360: "480x360",
    p480: "858x480",
    p720: "1280x720"
}

const convertVideo = (filename, coursesId) => {
    return new Promise(function (resolve, reject) {
        let fileConvert = {p360:'',p480:'',p720:''};
        

        var process = new ffmpeg(`./public/video/${coursesId}/${filename}`);
        process.then(function (video) {
            video
                .setVideoSize(HD.p360, true, true)
                .save(`./public/video/${coursesId}/video-courses-${coursesId}-p360.mp4`, function (error, file360) {
                    if (error) reject(error)

                    fileConvert.p360 = `video-courses-${coursesId}-p360.mp4`;

                    video
                        .setVideoSize(HD.p480, true, true)
                        .save(`./public/video/${coursesId}/video-courses-${coursesId}-p480.mp4`, function (error, file480) {
                            if (error) reject(error)

                            fileConvert.p480 = `video-courses-${coursesId}-p480.mp4`;

                            video
                                .setVideoSize(HD.p720, true, true)
                                .save(`./public/video/${coursesId}/video-courses-${coursesId}-p720.mp4`, function (error, file720) {
                                    if (error) reject(error)

                                    fileConvert.p720 = `video-courses-${coursesId}-p720.mp4`;
                                    resolve(fileConvert)
                                });
                        });
                });        
        }).catch(error => {
            reject(error)
        });
    })
}

const createSnapshot = (filename, coursesId) => {
    return new Promise(function (resolve, reject) {
        let output = {name:'',duration:0};
        var process = new ffmpeg(`./public/video/${coursesId}/${filename}`);
        process.then(function (video) {
            // console.log('createSnapshot',video)
            output.duration = video.metadata.duration.seconds;
            // Callback mode
            video.fnExtractFrameToJPG(`./public/video/${coursesId}`, {
                frame_rate: 1,
                number: 1,
                file_name: `thumbnail-courses-${coursesId}`
            }, function (error, files) {
                if (error) reject(error);

                output.name = `thumbnail-courses-${coursesId}_1.jpg`;
                resolve(output)
            });
        }, function (error) {
            reject(error);
        });
    });
}

const scanFolder = (coursesId) => {
    return new Promise(function (resolve, reject) {
        fs.readdir('./public/video', (error, files) => {
            if (error) reject('scan folder error' + error)
            files.forEach(file => {
                if (file === coursesId) {
                    resolve(true);
                }
            });
            resolve(false);
        });
    });
}

const createFolder = (path) => {
    return new Promise(function (resolve, reject) {
        fs.mkdir(path, 0777, function (err) {
            if (err) reject(false)
            resolve(true)
        })
    });
}

const uploadFile = (file, coursesId, filename) => {
    return new Promise(function (resolve, reject) {
        file.mv(`./public/video/${coursesId}/${filename}`, function (err) {
            if (err) reject(err)
            resolve(true);
        }, function (error) {
            reject(error)
        });
    });

}

module.exports.convertVideo = convertVideo;
module.exports.createSnapshot = createSnapshot;
module.exports.scanFolder = scanFolder;
module.exports.createFolder = createFolder;
module.exports.uploadFile = uploadFile;


