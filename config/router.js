const express = require("express");
const fileUpload = require('express-fileupload');

const app = express();

const bodyParser = require("body-parser");
const cors = require("cors");

app.use(express.static('public'))
app.use(cors());
app.use(bodyParser.json({limit: '100mb'}));
app.use(fileUpload());

const auth = require('../middlewares/Auth');
const testApi = require('../api/test');
const login = require('../api/Login');
const logout = require('../api/Logout');
const VideoUpload = require('../api/VideoUpload');


app.post("/login",auth.middleWare, login)
app.post("/logout", auth.requireJWTAuth,logout);
app.post("/video-upload", auth.requireJWTAuth,VideoUpload);

module.exports = app;