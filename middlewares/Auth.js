const passport = require("passport");
const ExtractJwt = require("passport-jwt").ExtractJwt;
const JwtStrategy = require("passport-jwt").Strategy;
const user = require("../database/Users");
const SECRET_KEY = process.env.SECRET_KEY;

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: SECRET_KEY
};

const jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
  if (payload.sub === SECRET_KEY) done(null, true);
  else done(null, false);
});

passport.use(jwtAuth);

const requireJWTAuth = passport.authenticate("jwt", { session: false });

const middleWare = (req, res, next) => {  
  user
    .checkLogin(req.body.email, req.body.password)
    .then(userInfo => {
      if (userInfo[0].hasUser > 0) {
        next();
      }else{
        res.send({
          status:403,
          error:error,
          message:"Wrong username and password"
        }); 
      }
    })
    .catch(error => {
      res.send({
        status:403,
        error:error,
        message:"Wrong username and password"
      });  
    });
};

module.exports.requireJWTAuth = requireJWTAuth;
module.exports.middleWare = middleWare;